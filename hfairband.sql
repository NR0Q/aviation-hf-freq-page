-- MariaDB dump 10.18  Distrib 10.5.8-MariaDB, for FreeBSD12.1 (amd64)
--
-- Host: localhost    Database: HFAirband
-- ------------------------------------------------------
-- Server version	10.5.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `freq`
--

DROP TABLE IF EXISTS `freq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freq` (
  `freq` int(11) NOT NULL,
  PRIMARY KEY (`freq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freq`
--

LOCK TABLES `freq` WRITE;
/*!40000 ALTER TABLE `freq` DISABLE KEYS */;
INSERT INTO `freq` VALUES (2854),(2860),(2869),(2872),(2881),(2887),(2890),(2899),(2944),(2962),(2964),(2965),(2971),(2989),(2992),(2998),(3007),(3016),(3413),(3446),(3452),(3455),(3458),(3467),(3470),(3473),(3476),(3485),(3488),(3491),(4651),(4666),(4675),(4684),(4712),(5460),(5481),(5484),(5520),(5547),(5550),(5559),(5565),(5568),(5574),(5577),(5583),(5598),(5616),(5628),(5634),(5637),(5643),(5649),(5652),(5655),(5670),(6529),(6532),(6535),(6544),(6547),(6550),(6555),(6556),(6571),(6577),(6586),(6589),(6595),(6622),(6628),(6631),(6637),(6655),(6667),(6673),(6692),(8825),(8831),(8837),(8843),(8846),(8861),(8864),(8867),(8870),(8879),(8891),(8897),(8903),(8906),(8915),(8918),(8942),(8951),(8954),(10021),(10036),(10042),(10048),(10057),(10066),(10072),(11276),(11279),(11282),(11285),(11291),(11309),(11330),(11336),(11363),(11384),(11390),(11396),(13261),(13288),(13291),(13297),(13300),(13303),(13306),(13309),(13315),(13318),(13339),(13354),(13357),(17904),(17907),(17940),(17946),(17952),(17955),(17961),(21925),(21964),(21985);
/*!40000 ALTER TABLE `freq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freqassign`
--

DROP TABLE IF EXISTS `freqassign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freqassign` (
  `regionID` varchar(6) NOT NULL,
  `freq` int(11) NOT NULL,
  PRIMARY KEY (`regionID`,`freq`),
  KEY `freq` (`freq`),
  CONSTRAINT `freqassign_ibfk_1` FOREIGN KEY (`regionID`) REFERENCES `region` (`regionID`),
  CONSTRAINT `freqassign_ibfk_2` FOREIGN KEY (`freq`) REFERENCES `freq` (`freq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freqassign`
--

LOCK TABLES `freqassign` WRITE;
/*!40000 ALTER TABLE `freqassign` DISABLE KEYS */;
INSERT INTO `freqassign` VALUES ('CAR-A',2887),('CAR-A',3455),('CAR-A',5550),('CAR-A',6577),('CAR-A',8846),('CAR-A',11396),('CAR-B',5520),('CAR-B',6586),('CAR-B',8918),('CAR-B',11330),('CAR-B',13297),('CAR-B',17907),('CAR-B',21985),('CEP-1',3413),('CEP-1',5574),('CEP-1',8843),('CEP-1',8915),('CEP-1',13354),('CEP-2',2869),('CEP-2',3452),('CEP-2',5547),('CEP-2',6673),('CEP-2',10057),('CEP-2',11282),('CEP-2',13288),('CEP-2',21964),('CEP-3',2869),('CEP-3',3452),('CEP-3',5547),('CEP-3',6673),('CEP-3',10057),('CEP-3',11282),('CEP-3',13288),('CEP-3',21964),('CWP-1',2998),('CWP-1',3455),('CWP-1',4666),('CWP-1',6532),('CWP-1',8903),('CWP-1',11384),('CWP-1',13300),('CWP-1',17904),('CWP-2',2998),('CWP-2',5652),('CWP-2',6532),('CWP-2',11384),('CWP-2',13300),('CWP-2',17904),('CWP-2',21985),('EA',3016),('EA',3485),('EA',3491),('EA',5655),('EA',5670),('EA',6571),('EA',8897),('EA',10042),('EA',11396),('EA',13297),('EA',13303),('EA',13309),('EA',17907),('INO',3476),('INO',5634),('INO',8879),('INO',13306),('INO',17961),('NAT-A',3016),('NAT-A',5598),('NAT-A',8906),('NAT-A',11396),('NAT-A',13306),('NAT-A',17946),('NAT-A',21925),('NAT-B',2899),('NAT-B',5616),('NAT-B',8864),('NAT-B',13291),('NAT-C',2872),('NAT-C',5649),('NAT-C',8879),('NAT-C',11336),('NAT-D',2971),('NAT-D',4675),('NAT-D',8891),('NAT-D',13291),('NAT-E',2962),('NAT-E',6628),('NAT-E',11309),('NAT-E',13354),('NAT-E',17952),('NAT-E',21964),('NAT-F',3476),('NAT-F',6622),('NAT-F',8831),('NAT-F',13291),('NAT-H',3491),('NAT-H',6667),('NAT-H',10021),('NAT-I',2890),('NAT-I',6595),('NAT-J',3446),('NAT-J',6547),('NCA',2964),('NCA',4712),('NCA',6529),('NCA',6544),('NCA',6589),('NCA',6631),('NCA',6692),('NCA',8837),('NCA',11390),('NP',5628),('NP',6655),('NP',8951),('NP',10048),('NP',13339),('NP',17946),('NP',21925),('RDAR10',2944),('RDAR10',3446),('RDAR10',4651),('RDAR10',5460),('RDAR10',5481),('RDAR10',5559),('RDAR10',5577),('RDAR10',6547),('RDAR1B',2890),('RDAR1B',5484),('RDAR1B',5568),('RDAR1B',6550),('RDAR1B',6595),('RDAR1D',2989),('RDAR1D',5637),('RDAR1E',3491),('RDAR1E',5583),('RDAR1E',6667),('RDAR1E',10021),('RDAR1E',10036),('SAT-1',3452),('SAT-1',6535),('SAT-1',8861),('SAT-1',13357),('SAT-1',17955),('SAT-2',2854),('SAT-2',5565),('SAT-2',11291),('SAT-2',13315),('SAT-2',17955),('SEA-1',3470),('SEA-1',5670),('SEA-1',6556),('SEA-1',10066),('SEA-1',11285),('SEA-1',13318),('SEA-1',17907),('SEA-2',3485),('SEA-2',5649),('SEA-2',5655),('SEA-2',8942),('SEA-2',11396),('SEA-2',13309),('SEA-2',17907),('SEA-3',3470),('SEA-3',6556),('SEA-3',11396),('SEA-3',13318),('SEA-3',17907),('SP',3467),('SP',5643),('SP',8867),('SP',13261),('SP',17904),('SPLDOC',3007),('SPLDOC',6637),('SPLDOC',10072),('SPLDOC',13339),('SPLDOC',17940);
/*!40000 ALTER TABLE `freqassign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `regionID` varchar(6) NOT NULL,
  `regionName` varchar(25) DEFAULT NULL,
  `regionDesc` text DEFAULT NULL,
  PRIMARY KEY (`regionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES ('CAR-A','Caribbean A',''),('CAR-B','Caribbean B',''),('CEP-1','Cent East Pacific 1',''),('CEP-2','Cent East Pacific 2',''),('CEP-3','Cent East Pacific 3',''),('CWP-1','Cent West Pacific 1',''),('CWP-2','Cent West Pacific 2',''),('EA','East Asia',''),('INO','Indian Ocean',''),('NAT-A','N Atlantic A','[Routes between 43N and 47N] [During OFF-PEAK hours, primary assignment for SOUTHERLY routes]'),('NAT-B','N Atlantic B','[Routes between 47N and 64N] [Assignments to aircraft flying EASTBOUND or WESTBOUND tracks] [Primary for CENTRAL routes]'),('NAT-C','N Atlantic C','[Routes between 47N and 64N] [Assignments to aircraft flying EASTBOUND or WESTBOUND tracks] [Primary for CENTRAL routes]'),('NAT-D','N Atlantic D','[Routes north of 62N] [During OFF-PEAK hours, primary assignment for NORTHERLY routes]'),('NAT-E','N Atlantic E','[Routes between New York and Santa Maria, south of 43N] [Unguarded in OFF-PEAK hours]'),('NAT-F','N Atlantic F','[Routes within Gander and Shanwick areas only]'),('NAT-H','N Atlantic H','[Routes assigned to aircraft flying entirely within Santa Maria area]'),('NAT-I','N Atlantic I',''),('NAT-J','N Atlantic J',''),('NCA','North Central Asia',''),('NP','North Pacific',''),('RDAR10','Reg-Dom Air Route 10E',''),('RDAR1B','Reg-Dom Air Route 1B',''),('RDAR1D','Reg-Dom Air Route 1D',''),('RDAR1E','Reg-Dom Air Route 1/1E',''),('SAT-1','S Atlantic 1',''),('SAT-2','S Atlantic 2',''),('SEA-1','Southeast Asia 1',''),('SEA-2','Southeast Asia 2',''),('SEA-3','Southeast Asia 3',''),('SP','South Pacific',''),('SPLDOC','South Pac Long Dist Ctl','');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regsta`
--

DROP TABLE IF EXISTS `regsta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regsta` (
  `regionID` varchar(6) NOT NULL,
  `stationID` int(11) NOT NULL,
  PRIMARY KEY (`regionID`,`stationID`),
  KEY `stationID` (`stationID`),
  CONSTRAINT `regsta_ibfk_1` FOREIGN KEY (`regionID`) REFERENCES `region` (`regionID`),
  CONSTRAINT `regsta_ibfk_2` FOREIGN KEY (`stationID`) REFERENCES `stations` (`stationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regsta`
--

LOCK TABLES `regsta` WRITE;
/*!40000 ALTER TABLE `regsta` DISABLE KEYS */;
INSERT INTO `regsta` VALUES ('CAR-A',1),('CAR-B',1),('CEP-1',18),('CEP-2',18),('CEP-3',18),('CWP-1',18),('CWP-1',19),('CWP-1',20),('CWP-2',18),('CWP-2',19),('CWP-2',21),('EA',27),('EA',28),('EA',29),('EA',30),('INO',36),('INO',38),('NAT-A',1),('NAT-A',2),('NAT-A',3),('NAT-A',4),('NAT-B',2),('NAT-B',3),('NAT-B',5),('NAT-C',2),('NAT-C',3),('NAT-C',5),('NAT-D',2),('NAT-D',3),('NAT-D',5),('NAT-D',6),('NAT-D',7),('NAT-E',1),('NAT-E',4),('NAT-F',2),('NAT-F',3),('NAT-H',4),('NAT-I',3),('NAT-J',3),('NCA',31),('NCA',32),('NCA',33),('NCA',34),('NCA',35),('NP',18),('NP',19),('SAT-1',8),('SAT-1',9),('SAT-1',10),('SAT-1',11),('SAT-1',12),('SAT-1',13),('SAT-1',14),('SAT-1',15),('SAT-1',16),('SAT-2',8),('SAT-2',10),('SAT-2',11),('SAT-2',12),('SAT-2',13),('SAT-2',14),('SAT-2',15),('SAT-2',16),('SAT-2',17),('SEA-1',36),('SEA-1',37),('SEA-1',38),('SEA-2',19),('SEA-2',39),('SEA-2',40),('SEA-2',41),('SEA-3',23),('SEA-3',40),('SEA-3',41),('SP',18),('SP',22),('SP',23),('SP',24),('SP',25),('SP',26);
/*!40000 ALTER TABLE `regsta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stations`
--

DROP TABLE IF EXISTS `stations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stations` (
  `stationID` int(11) NOT NULL,
  `stationName` varchar(25) NOT NULL,
  PRIMARY KEY (`stationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stations`
--

LOCK TABLES `stations` WRITE;
/*!40000 ALTER TABLE `stations` DISABLE KEYS */;
INSERT INTO `stations` VALUES (1,'New York Radio'),(2,'Gander Radio'),(3,'Shanwick Radio'),(4,'Santa Maria Radio'),(5,'Iceland Radio'),(6,'Arctic Radio'),(7,'Bodo Radio'),(8,'Canarias Radio'),(9,'Brasilia Radio'),(10,'Cayenne Radio'),(11,'Dakar Radio'),(12,'Manaus Radio'),(13,'Paramaribo Radio'),(14,'Recife Radio'),(15,'Rio de Janerio Radio'),(16,'Sal Island Radio'),(17,'Johannesburg Radio'),(18,'San Francisco Radio'),(19,'Tokyo Radio'),(20,'Manila Radio'),(21,'Guam Radio'),(22,'Auckland Radio'),(23,'Brisbane Radio'),(24,'Nadi Radio'),(25,'Tahiti Radio'),(26,'Clipperton Radio'),(27,'Beijing Radio'),(28,'Guangzhou Radio'),(29,'Pyongyang Radio'),(30,'Shanghai Radio'),(31,'Magadan Radio'),(32,'Irkutsk Radio'),(33,'Khabarovsk Radio'),(34,'Petropavlovsk Radio'),(35,'Syktyvkar Radio'),(36,'Bombay Radio'),(37,'Calcutta Radio'),(38,'Colombo Radio'),(39,'Bangkok Radio'),(40,'Jakarta Radio'),(41,'Singapore Radio');
/*!40000 ALTER TABLE `stations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-25 19:52:32
