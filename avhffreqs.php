<!DOCTYPE html>
<html>
<head>
	<title>Aviation HF Frequencies</title>
<style>
body {
   background-color: black;
   color: white;
}

h2 {
   color: red;
   margin-left: 30px;
}

h3 {
   color: yellow;
   margin-left: 40px;
}

div.desc {
   color: white;
   margin-left: 40px;
}

div.stations {
   margin-left: 45px;
   color: silver;
}

table.stations {
   margin-left: 45px;
   border-collapse: collapse;
   border: 1px solid silver;
   color: silver;
}

td.stations {
   border: 1px solid silver;
   padding: 5px;
}

div.freqs {
   margin-left: 45px;
   color: silver;
}

table.freqs {
   margin-left: 45px;
   border-collapse: collapse;
   border: 1px solid silver;
   color: white;
}

td.freqs {
   border: 1px solid silver;
   padding: 5px;
}

</style>
</head>
<body>
<h2>Aviation HF Frequencies</h2>
<?php

	$regions = dbQuery("SELECT * FROM region",array());
	foreach($regions as $row) {

		// Get and output region data
		echo "<h3>" . $row["regionName"] . "</h3>\n";
		echo "<div class='desc'>" . $row["regionDesc"] . "</div>\n";

		// Get and output station data
		$stations = dbQuery("SELECT stations.stationName FROM regsta INNER JOIN stations ON regsta.stationID=stations.stationID WHERE regsta.regionID=:regID",array(":regID"=>$row["regionID"]));
		echo "<div class='stations'>Stations: \n" . "<table class='stations'>\n<tr>\n";
		foreach($stations as $station) {
			echo "<td class='stations'>" . $station["stationName"] . "</td>\n";
		}
		echo "</tr>\n</table>\n</div>\n";

		// Get and output frequency data
		$freqs = dbQuery("SELECT freq FROM freqassign WHERE regionID=:regID",array(":regID"=>$row["regionID"]));
		echo "<div class='freqs'>Frequencies (in kHz): \n" . "<table class='freqs'>\n<tr>\n";
		foreach($freqs as $freq) {
			echo "<td class='freqs'>" . $freq["freq"] . "</td>\n";
		}
		echo "</tr>\n</table>\n</div>\n\n";		
	}

	function dbQuery($sql, $sqlargs) {
		$servername = "localhost";
		$username = "user"; 
		$password = "password";
		try {
			$conn = new PDO("mysql:host=$servername;dbname=HFAirband", $username, $password);
			
			// set the PDO error mode ot exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $conn->prepare($sql);
			
			// check if there are any args and execute as appropriate
			if(empty($sqlargs)) {
				$stmt->execute();
			} else {
				$stmt->execute($sqlargs);
			}
			
			// return results as assoc array
			$result = $stmt->fetchAll();
			return $result;
		} catch(PDOException $e) {
			echo "Database Error: " . $e->getMessage();
		}
		$conn = null;
	}	
?>
</body>
</html>
